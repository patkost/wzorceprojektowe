package structural.composite.containers;

import structural.composite.PurchaseAble;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Box {
    private BigDecimal boxPrice = new BigDecimal("1");
    private List<PurchaseAble> productsInside = new ArrayList<>();
    public void addProduct(PurchaseAble product){
        productsInside.add(product);
    }
}
