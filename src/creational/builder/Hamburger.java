package creational.builder;

public class Hamburger implements Meal{
    private Bacon bacon;
    private Cheese cheese;
    private Lettuce lettuce;
    private Sauce sauce;
    private boolean salty;
    private boolean spicy;
    private boolean extraCheese;
    private BasicRoll roll;
    private Meat meat;

    public Hamburger(Bacon bacon, Cheese cheese, Lettuce lettuce, Sauce sauce, boolean salty, boolean spicy, boolean extraCheese, BasicRoll roll, Meat meat) {
        this.bacon = bacon;
        this.cheese = cheese;
        this.lettuce = lettuce;
        this.sauce = sauce;
        this.salty = salty;
        this.spicy = spicy;
        this.extraCheese = extraCheese;
        this.roll = roll;
        this.meat = meat;
    }

    @Override
    public String toString() {
        return "Hamburger{" +
                "bacon=" + bacon +
                ", cheese=" + cheese +
                ", lettuce=" + lettuce +
                ", sauce=" + sauce +
                ", salty=" + salty +
                ", spicy=" + spicy +
                ", extraCheese=" + extraCheese +
                ", roll=" + roll +
                ", meat=" + meat +
                '}';
    }

    @Override
    public void eat() {
        System.out.println("Eat Hamburger!" + toString());
    }
}
