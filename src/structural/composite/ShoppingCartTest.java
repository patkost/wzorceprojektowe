package structural.composite;

import org.junit.jupiter.api.Test;
import structural.composite.products.Chocolate;
import structural.composite.products.Cola;
import structural.composite.products.Cookies;
import structural.composite.products.Snack;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShoppingCartTest {
    @Test
    public void validateShoppingCartTotalPrice(){
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cola());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Snack());
        shoppingCart.addProduct(new Chocolate());
        shoppingCart.addProduct(new Chocolate());

        //when

        //then
        assertEquals(new BigDecimal("23"),shoppingCart.getTotalCost());
    }

    @Test
    public void validateShoppingCartTotalPriceIncludingBoxes(){
        // given
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Cookies());
        shoppingCart.addProduct(new Snack());
        // TODO: Dodaj do shoppingCart obiekt Box z 2 czekoladami oraz drugi Box z 4 colami

        //when

        //then
        assertEquals(new BigDecimal("27"),shoppingCart.getTotalCost());
    }
}
