package structural.facade;

public class EngineCooler {
    public void setCoolerMode(boolean cooling){
        System.out.println(cooling ? "Starting to cool the engine!" : "Cooling disabled!");
    }
}
