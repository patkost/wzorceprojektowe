package structural.flyweight;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ColoredPointsTest
{
    @Test
    public void createThousandPointsTest(){
        List<ColoredPoint> points = new ArrayList<>();
        Random random = new Random();
        for (int x = 0; x < 2000; x++) {
            for (int y = 0; y < 2000; y++) {
                ColorType randomColorType = ColorType.values()[random.nextInt(ColorType.values().length)];
                Color randomColor = ColorFactory.retrieveColor(randomColorType);
                points.add(new ColoredPoint(randomColor,x,y));
            }
        }
        long heapSize = Runtime.getRuntime().totalMemory();
        long freeMem = Runtime.getRuntime().freeMemory();
        System.out.println("HeapSize is " + heapSize/1000000 + " MB, used memory is "+ (heapSize-freeMem)/1000000 + " MB");
    }
}
