#######################
####   Zadanie 4   ####
#######################

W poprzednim zadaniu zaimplementowałeś prostą metodę wytwórczą - w tym wytworzysz zwykłą metodę wytwórczą.
Na potrzeby tego zadania załóżmy, że zwykły mięsny Hamburger pochodzący z Hamburger.class będzie miał jedną słuszną postać
- wybraną przez Ciebie.

1) Stwórz nowy typ burgera na podobnej zasadzie jak w poprzednim zadaniu -> na przykład KidsBurger.class

2) Stwórz 3 klasy rozszerzające abstrakcję BurgerFactory.class:
 - NormalBurgerFactory (tworzy Hamburger.class)
 - VeganBurgerFactory (tworzy VeganBurger.class)
 - KidsBurgerFactory (tworzy KidsBurger.class)
 i zaimplementuj metodę prepareBurger() aby zwracała odpowiedni typ burgera

4) Stworz enum BurgerType z 3 wartosciami, ktore beda reprezentowaly typy burgera.
Uzupełnij w konstruktorze mapę dostępnymi fabrykami.


5)Zparsuj otrzymywany z konsoli wybór klienta do tej postaci i na tej podstawie wybierz odpowiednią fabrykę
z której wytworzysz docelowy produkt. Weź pod uwagę błędny wybór / domyślny
